<?php

namespace Hapi\Blog;

class Greeter
{
    public function greet(String $string)
    {
        return 'Hi, this is my first package blog. Welcome '.$string;
    }
}
