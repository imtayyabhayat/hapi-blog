<?php return array(
    'root' => array(
        'name' => 'hapi/blog',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '3e88b07a2128e51f8191faa373a17df321553c17',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'hapi/blog' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '3e88b07a2128e51f8191faa373a17df321553c17',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
